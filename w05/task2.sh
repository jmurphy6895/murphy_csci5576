#!/bin/bash

#SBATCH --time=0:05:00
#SBATCH -o axpy-%j.out
#SBATCH -e axpy-%j.err
#SBATCH --qos debug

# advise task manager that maximum of 4
# tasks/processes may be spawned
#SBATCH --ntasks 4

# run the program
./generate-array.exe 5 4 > array.txt
mpirun -n 4 ./systolic-matvec.exe array.txt