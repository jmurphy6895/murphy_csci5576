#!/bin/bash

#SBATCH --time=0:05:00
#SBATCH --nodes=1
#SBATCH -o pc-%j.out
#SBATCH -e pc-%j.err
#SBATCH --ntasks 16
#SBATCH --qos debug

# run the program
echo "Running on $(hostname --fqdn)"
module load intel

export OMP_NUM_THREADS=6

for i in {1..500}; do ./pc.exe 10 3 3 2>/dev/null;done |sort|uniq -c