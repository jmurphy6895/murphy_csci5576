#include "poisson.h"
#include "jacobi.h"
#include <iostream>
#include <cassert>
#include <math.h>

#define PI 3.14159265
#define k_max 10000

int main(int argc, char** argv){

    assert (argc>1);

    MPI_Init(&argc, &argv);

    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    double t1, t2;
    t1 = MPI_Wtime();

    if(rank == 0) {
        int size;
        MPI_Comm_size(MPI_COMM_WORLD, &size);
        std::cout<<"Number of Processors: "<<size<<std::endl;;
    }

    MPI_Comm grid_comm;

    int n = atoi(argv[1]);
    double eps_r = 1E-5;
    double eps_a = 1E-12;

    grid_t x;   

    poisson_setup(MPI_COMM_WORLD, n, grid_comm, x);

    // fill a and f based on x
    vec_t a, f;
    vec_t u0, u_final, diag;

    for(size_t i = 0; i < x.size(); ++i){
        a.push_back(12);
        f.push_back(sin(2*PI*x[i].x)*sin(12*PI*x[i].y)*sin(8*PI*x[i].z));
        u0.push_back(sin(4*PI*x[i].x)*sin(10*PI*x[i].y)*sin(14*PI*x[i].z));
    }

    // make a matvec object to pass to the residual function (residual
    // doesn't care how you do the matvec, it just passes an input and
    // expect and output.
    matvec_t mv = std::bind(poisson_matvec, std::ref(grid_comm), n, std::ref(a),
                                std::placeholders::_1, std::placeholders::_2);

    int k;

    for(size_t i = 0; i < x.size(); ++i){
        diag.push_back(-6);
    }

    jacobi_solver(grid_comm, mv, diag, f, u0, eps_r, eps_a, k_max, u_final, k);

    
    t2 = MPI_Wtime(); 
    if(rank == 0) {
        std::cout<<"Number of Iterations: "<<k<<std::endl;
        std::cout<<"Elapsed time is "<<t2 - t1<<std::endl;
    }

    // clean up
    MPI_Finalize();

    return 0;
}