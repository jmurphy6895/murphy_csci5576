#include <mpi.h>
#include <stdio.h>
#include "slurp_file.h"
#include <cassert>
#include <climits>

int main(int argc, char** argv){

	assert (argc>1);

	MPI_Init(&argc, &argv);

	int rank;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	int size;
	MPI_Comm_size(MPI_COMM_WORLD, &size);

	std::vector<int> data;

	slurp_file_line(argv[1], rank, data);

	int max = INT_MIN;

	for(size_t i = 0; i < data.size(); ++i){
		if(data[i] > max){
			max = data[i];
		}
	}

	printf("Maximum from row %d is %d\n", rank, max);

	int temp = INT_MIN;
	MPI_Status st;

	if(rank != size - 1){
		MPI_Recv(&temp, 1, MPI_INT, rank + 1, 0, MPI_COMM_WORLD, &st);
	}

	if (temp > max){
		max = temp;
	}

	if(rank != 0){
		MPI_Send(&max, 1, MPI_INT, rank - 1, 0, MPI_COMM_WORLD);
	}

	
	if(rank == 0) {
		printf("Maximum from the array is %d\n", max);
	}

	MPI_Finalize();

	return 0;

}