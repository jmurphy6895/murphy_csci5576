#!/bin/bash

#SBATCH --time=0:30:00
#SBATCH -o jacobi-%j.out
#SBATCH -e jacobi-%j.err
#SBATCH --qos debug

# advise task manager that maximum of 64
# tasks/processes may be spawned
#SBATCH --ntasks 64

# run the program
mpirun -n 64 ./test_jacobi.exe 512