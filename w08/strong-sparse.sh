#!/bin/bash

#SBATCH --time=0:30:00
#SBATCH -o weak-sparse-%j.out
#SBATCH -e weak-sparse-%j.err
#SBATCH --qos debug

# advise task manager that maximum of 25
# tasks/processes may be spawned
#SBATCH --ntasks 25

# run the program
for i in {1, 2, 4, 5, 10, 20, 25};do mpirun -n i ./test_sparse.exe 100 150 3750;done
