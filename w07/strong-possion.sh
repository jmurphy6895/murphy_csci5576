#!/bin/bash

#SBATCH --time=0:30:00
#SBATCH -o possion-%j.out
#SBATCH -e possion-%j.err
#SBATCH --qos debug

# advise task manager that maximum of 27
# tasks/processes may be spawned
#SBATCH --ntasks 27

# run the program
for i in {1,8,27,4,9,16,25};do mpirun -n i ./test_poisson.exe 240;done
