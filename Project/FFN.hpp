//
//  FFN.hpp
//  Neural Networks
//
//  Created by Alexis Louis on 15/03/2016.
//  Copyright © 2016 Alexis Louis. All rights reserved.
//



#ifndef FFN_hpp
#define FFN_hpp

#include "Header.h"

using namespace std;
class Layer;

class FFN{
    
public:
    FFN();
    
    void initFFN(int nb_inputs, int nb_hidden_neurons, int nb_outputs);
    void sim(vector<double> inputs);
    void test(vector<vector<double>> Xtest, vector<vector<double>> Tt, vector<double> &mean_error);
    void train_by_iteration(vector<vector<double>> inputs, vector<vector<double>> targets, int target_iteration);
    void train_by_error(vector<vector<double>> inputs, vector<vector<double>> targets, double target_error);
    
    void about();
    
    vector<double> get_ffn_outputs();
    void set_targets(vector<double> tar);
    vector<double> get_targets(void){return targets;};
    int get_nb_layers(){return layers.size();};
    Layer* get_layer_at(int indice){return layers[indice];};

    void show_full_weights_matrix();
    void get_weights_matrix(vector<vector<double>> &weights, int i);
    

private:
    vector<Layer*> layers;
    vector<double> targets;
    void add_layer(Layer* l);
};

#endif /* FFN_hpp */
