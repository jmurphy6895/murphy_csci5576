#!/bin/bash

#SBATCH --time=0:05:00
#SBATCH --nodes=1
#SBATCH -o fibonacci-%j.out
#SBATCH -e fibonacci-%j.err
#SBATCH --ntasks 16
#SBATCH --qos debug

# run the program
echo "Running on $(hostname --fqdn)"
module load intel

export OMP_NUM_THREADS=1

./test_quicksort.exe 1000000
./test_quicksort.exe 10000000
