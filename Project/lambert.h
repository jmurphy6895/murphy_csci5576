#ifndef ___LAMBERT__
#define ___LAMBERT__

#include <vector>
#include <algorithm>
#include <iostream>
#include <mpi.h>
#include <random>
#include "aux_data.h"

#define PI 3.14159265

/*
 *  Creates Random Selection of Position Vectors(r1, r2), Gravitational Parameters(u), and Times(t) used to Calculate(M)
 */ 
void generate_Lambert_data(std::vector<double> &u, std::vector<pos_vec> &r1, std::vector<pos_vec> &r2, std::vector<double> &t, std::vector<pos_vec> &v, const int n){

	std::random_device rd;

    std::mt19937 e2_u(rd());

    std::mt19937 e2_r1_x(rd());
    std::mt19937 e2_r1_y(rd());
    std::mt19937 e2_r1_z(rd());

    std::mt19937 e2_r2_x(rd());
    std::mt19937 e2_r2_y(rd());
    std::mt19937 e2_r2_z(rd());

    std::mt19937 e2_t(rd());

    std::uniform_real_distribution<> dist(0, 1);

    for(size_t i; i < n; ++i){
		
		u.push_back(dist(e2_u));

		pos_vec position1, position2;

		position1.x = 4 * dist(e2_r1_x) + 1E-4;
		position1.y = 4 * dist(e2_r1_y) + 1E-4;
		position1.z = 4 * dist(e2_r1_z) + 1E-4;
		r1.push_back(position1);

		position2.x = 4 * dist(e2_r2_x) + 1E-4;
		position2.y = 4 * dist(e2_r2_y) + 1E-4;
		position2.z = 4 * dist(e2_r2_z) + 1E-4;
		r2.push_back(position2);

		double r1mag = dot(r1[i],r1[i]);
		double r2mag = dot(r2[i],r2[i]);

		double thetaS = arccos(dot(r1[i], r2[i])/(r1[i] * r2[i]))

		double c = sqrt(pow(r1[i], 2) + pow(r2[i], 2) - (2 * r1[i] * r2[i] * cos(thetaS)));
    	double s = (r1[i] + r2[i] + c) / 2;

		t.push_back(5 * dist(e2_r2) + 3);
		
		double t_parabolic;

		if (sin(thetaS) > 0){
        	t_parabolic = (sqrt(2) / (3 * sqrt(u))) * (pow(s, 1.5) - pow((s-c), 1.5));
   		} 
   		else{
        	t_parabolic = (sqrt(2) / (3 * sqrt(u))) * (pow(s, 1.5) + pow((s-c), 1.5));
		}

		if (t[i] < t_parabolic){
			t[i] = t_parabolic * 2;
		}

		double alpha_m = PI;
		double beta_m = 2 * arcsin(sqrt((s - c) / s));
		double t_m = sqrt(1/u[i]) * sqrt(pow(s, 3) / 8) * (alpha_m - beta_m + sin(beta_m));

		double a;
		if (t <= t_m){
			a = bisection(s, c, t[i], u[i]);
		}
		else{
			a = bisection2(s, c, t[i], u[i]);
		}

		pos_vec r1_hat, chat;
		for (size_t j = 0; j < r1[i].size(); ++j){
			r1_hat[j] = r1[i][j] / dot(r1[i])
			chat[j] = r2[i][j] - r1[i][j];
		}

		double cmag = dot(chat, chat);
		for (size_t j = 0; j < r1[i].size(); ++j){
			chat[j] = chat[j] / cmag;
		}

		double alpha = 2 * arcsin(sqrt(s / (2 * a)));
		if(t > t_m){
			alpha = 2 * PI - alpha;
		}

		double beta = -2 * arcsin(sqrt((s - c) / (2 * a)));

		double A = sqrt(u / (4 * a)) * 1 / tan(alpha / 2);
		double B = sqrt(u / (4 * a)) * 1 / tan(beta / 2);

		pos_vec vS;
		for (size_t j = 0; j < r1[i].size(); ++j){
			vS[j] = (B + A) * chat[j] + (B - A) * r1_hat[j];
		}

		v.push_back(vS);

	}

};


double dot(const pos_vec r1, const pos_vec r2){

	double sum = 0;

	for(size_t i = 0; i < r1.size(); ++i){
		sum += r1[i]*r2[i];
	}

	return sum;
};

double bisection(const double s, const double c, const double t, const double u){
	double alpha = 2 * arcsin(sqrt(s / (2 * a)));
	double beta = -2 * arcsin(sqrt((s - c) / (2 * a)));

	double temp = (sqrt(u) * t) - pow(a, 1.5) * (alpha - beta - (sin(alpha) - sin(beta)));

	return;
};

double bisection2(const double s, const double c, const double t[i], const double u[i]){


	return;
};

#endif