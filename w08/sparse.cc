#include "sparse.h"

void sort_index(const std::vector<size_t> &v, std::vector<size_t> &idx){
    size_t n(0);
    idx.resize(v.size());
    std::generate(idx.begin(), idx.end(), [&]{ return n++; });
    std::sort(idx.begin(), idx.end(), 
              [&](size_t ii, size_t jj) { return v[ii] < v[jj]; } );
};

sparse::sparse(size_t m, size_t n, storage_type t) :
    m(m), n(n), type(t) {}

void sparse::transpose(sparse &B) const{

	// ASSUMES MATRIX IS IN COO FORM
	B.type = this->type;

	B.row = this->column;
	B.column = this->row;
	B.data = this->data;

	std::vector<size_t> idx;

	sort_index(B.row, idx);

	std::vector<data_t> local_data;
    std::vector<size_t> local_column;
    std::vector<size_t> local_row;

	for(size_t i = 0; i < idx.size(); ++i){
		local_data.push_back(B.data[idx[i]]);
		local_row.push_back(B.row[idx[i]]);
		local_column.push_back(B.column[idx[i]]);
	}

	//std::vector<data_t> rec_data;
    //std::vector<size_t> rec_column;
    //std::vector<size_t> rec_row;

	//MPI_Alltoallv(&local_row, /*sentcount*/ , /*senddisp*/, MPI_DOUBLE
	//			  &rec.row, /*receivecount*/, /*receivedisp*/,  MPI_DOUBLE, MPI_COMM_WORLD);
	//MPI_Alltoallv(&local_column, /*sentcount*/ , /*senddisp*/, MPI_DOUBLE
	//			  &rec.column, /*receivecount*/, /*receivedisp*/,  MPI_DOUBLE, MPI_COMM_WORLD);
	//MPI_Alltoallv(&local_data, /*sentcount*/ , /*senddisp*/, MPI_DOUBLE
	//			  &rec.data, /*receivecount*/, /*receivedisp*/,  MPI_DOUBLE, MPI_COMM_WORLD);

	B.row = local_row;
	B.column = local_column;
	B.data = local_data;

	return;
}

void sparse::fill_CSR(size_t nnz){

	size_t count = 0;

	for (size_t i = 0; i < this->m; ++i){

		this->row.push_back(count);

		for (size_t j = 0; j < this->n; ++j){

			if(rand() % 4 == 0){

				if(count < nnz){
					this->data.push_back(rand() % 100);
					this->column.push_back(j);
					count++;
				}
			}
		}
	}

	this->row.push_back(count);
	return;
}

void sparse::convert2COO(){

	this->type = COO;

	std::vector<data_t> local_data;
    std::vector<size_t> local_column;
    std::vector<size_t> local_row;

	for(size_t i = 0; i < this->row.size() - 1; ++i){
		for(size_t j = this->row[i]; j < this->row[i+1]; ++j){
			local_row.push_back(i);
			local_column.push_back(column[j]);
			local_data.push_back(data[j]);
		}
	}

	this->row = local_row;
	this->column = local_column;
	this->data = local_data;

	return;
}

void sparse::convert2CSR(){

	this->type = CSR;

	std::vector<data_t> local_data;
    std::vector<size_t> local_column;
    std::vector<size_t> local_row;

    local_row.push_back(0);

    for(size_t i = 0; i < this->data.size(); ++i){
    	if(this->row[i] == this->row[local_row[local_row.size() - 1]]){
    		local_data.push_back(this->data[i]);
    		local_column.push_back(this->column[i]);
    	}
    	else {
    		int zero_rows = this->row[i] - this->row[local_row[local_row.size() - 1]];
    		for(size_t j = 0; j < zero_rows; ++j){
    			local_row.push_back(i);
    		}
    		local_data.push_back(this->data[i]);
    		local_column.push_back(this->column[i]);
    	}

    }

    while(local_row.size() < this->m + 1){
    	local_row.push_back(this->data.size());
    }

    this->row = local_row;
	this->column = local_column;
	this->data = local_data;

	return;
}

void sparse::print() const{

	int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    std::cout << "Processor: " << rank << std::endl;

	if(this->type == CSR){
		this->print_CSR();
	}
	else{
		this->print_COO();
	}

	return;
}

void sparse::print_CSR() const{

	std::cout << "Print CSR" << std::endl;
	std::cout << "Data" << "(" << this->data.size() << "): ";

	for (size_t i = 0; i < this->data.size(); ++i){
		if(i != this->data.size() - 1){
			std::cout << this->data[i] << ", ";
		} else{
			std::cout << this->data[i] << std::endl;
		}
	}


	std::cout << "Column" << "(" << this->column.size() << "): ";

	for (size_t i = 0; i < this->column.size(); ++i){
		if(i != this->column.size() - 1){
			std::cout << this->column[i] << ", ";
		} else{
			std::cout << this->column[i] << std::endl;
		}
	}

	std::cout << "Row" << "(" << this->row.size() << "): ";

	for (size_t i = 0; i < this->row.size(); ++i){
		if(i != this->row.size() - 1){
			std::cout << this->row[i] << ", ";
		} else{
			std::cout << this->row[i] << std::endl;
		}
	}

	return;
}

void sparse::print_COO() const{
	std::cout << "Print COO" << std::endl;
	std::cout << "Data Size: " << this->data.size() << std::endl;

	for (size_t i = 0; i < this->row.size(); ++i){
		std::cout << "(" << this->row[i] << ", " << this->column[i] << ") " << this->data[i] << std::endl;
	}

	return;
}
