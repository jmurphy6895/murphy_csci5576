#ifndef ___SPARSE__MAT__
#define ___SPARSE__MAT__

#include <vector>
#include <algorithm>
#include <iostream>
#include <mpi.h>

typedef double data_t;

/*
 *  Returns the shuffling index set such that v[idx] is sorted
 */ 
void sort_index(const std::vector<size_t> &v, std::vector<size_t> &idx);

class sparse {
  public:
    enum storage_type {CSR, COO};
    std::vector<data_t> data;
    std::vector<size_t> column;
    std::vector<size_t> row;

    size_t m,n; /* (local) matrix dimensions */
    storage_type type;
    sparse(size_t m, size_t n, storage_type t = CSR);
    void transpose(sparse &B) const; /* returns transpose in B */
    void fill_CSR(size_t nnz);
    void convert2COO();
    void convert2CSR();
    void print() const;
    void print_CSR() const;
    void print_COO() const;
};

#endif