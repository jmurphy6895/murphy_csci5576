#include "quicksort.h"

/*
 * Based on Lomuto Partition Scheme
 * https://en.wikipedia.org/wiki/Quicksort
 */

void quicksort(const vec_t &x, vec_t &y){
	
	#pragma omp parallel shared(y)
	{
		#pragma omp for
		for (int i = 0; i < x.size(); i++)
		{
			y[i] = x[i];
		}

		#pragma omp single
		quicksort_helper(y, 0, x.size() - 1);
	}
}


void quicksort_helper(vec_t &y, int lo, int hi){

	if(lo < hi){

		long p = partition(y, lo, hi);

		if(hi - lo > 8192){
			#pragma omp task shared(y)
			quicksort_helper(y, lo, p - 1);

			#pragma omp task shared(y)
			quicksort_helper(y, p + 1, hi);
		} else {
			quicksort_helper(y, lo, p - 1);
			quicksort_helper(y, p + 1, hi);
		}

	}

}

int partition(vec_t &y, int lo, int hi){

	long pivot = y[hi];
	int i = lo;

	for(int j = lo; j < hi; ++j){
		if(y[j] < pivot){
			long temp = y[i];
			y[i] = y[j];
			y[j] = temp;
			i++;
		}
	}

	long temp = y[i];
	y[i] = y[hi];
	y[hi] = y[i];

	return i;

}

