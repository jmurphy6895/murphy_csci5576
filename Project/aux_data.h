#ifndef ___DATA_STRUCTS___
#define ___DATA_STRUCTS___

union pos_vec
{
    double coord[3];
    struct {
        double x;
        double y;
        double z;
    };
};

#endif