#!/bin/bash

#SBATCH --time=0:30:00
#SBATCH -o strong-sparse-%j.out
#SBATCH -e strong-sparse-%j.err
#SBATCH --qos debug

# advise task manager that maximum of 27
# tasks/processes may be spawned
#SBATCH --ntasks 27

# run the program
mpirun -n 1 ./test_sparse.exe 6 5 8