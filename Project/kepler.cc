#include "kepler.h"

void generate_Kepler_data(vector<vector<double>> &train_x, vector<vector<double>> &train_y, vector<vector<double>> &test_x, vector<vector<double>> &test_y, const int n){

	std::random_device rd;

    std::mt19937 e2_E(rd());
    std::mt19937 e2_e(rd());

    std::uniform_real_distribution<> dist(0, 1);

    double E, e, M;

    vector<double> x_zeros(2,0);
    vector<double> y_zeros(1,0);

	for(size_t i = 0; i < n; ++i){
		E = 2 * PI * dist(e2_E);
		e = dist(e2_e) * .5;

		M = E - e * sin(E);

        M = fmod(M, 2.0*PI);

		if(i < (2 / 3) * n){
            train_x.push_back(x_zeros);
			train_x[i][0] = M;
            train_x[i][1] = e;

            train_y.push_back(y_zeros);
            train_y[i][0] = E;
		} else {
            int j = i - ((2 / 3) * n);

			test_x.push_back(x_zeros);
            test_x[j][0] = M;
            test_x[j][1] = e;

            test_y.push_back(y_zeros);
            test_y[j][0] = E;
		}
	}

}