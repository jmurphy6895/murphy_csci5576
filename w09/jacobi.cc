#include "jacobi.h"
#include <iostream>
#include <cstdio>
#include <cmath>
#include <limits>

void jacobi_solver(const MPI_Comm &comm, matvec_t &mv,
std::vector<real_t> &diag, std::vector<real_t> &rhs,
std::vector<real_t> &u0, real_t eps_r, real_t eps_a, int k_max,
std::vector<real_t> &u_final, int &k){

	real_t res_norm, res_norm_u0;
	vec_t res, res_u0;

	residual(MPI_COMM_WORLD, mv, u0, rhs, res_u0, res_norm_u0);

	u_final = u0;

	for (k = 0; k < k_max; k++) {

		residual(MPI_COMM_WORLD, mv, u_final, rhs, res, res_norm);

		// D = diag(L) = -6 * I
		// D*u =  -6 * I * u = -6*u

		// D^-1 = -1/6 * I
		// u+ = D^-1[r + D*u]
		// u+ = -1/6 * I * [r + -6*u] = -1/6 * [r + -6*u]

		// Thus matrix blocks don't need to communicate during update
		// Only during residual calulation

		for (size_t i = 0; i < u_final.size(); ++i){
			u_final[i] += (1/diag[i]) * (res[i] + diag[i]*u_final[i]);
		}

		if (res_norm < (eps_r*res_norm_u0) + eps_a){
			k++;
			return;
		}

	}

	k++;
	return;
}