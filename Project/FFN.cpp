//
//  FFN.cpp
//  Neural Networks
//
//  Created by Alexis Louis on 15/03/2016.
//  Copyright © 2016 Alexis Louis. All rights reserved.
//

#include "FFN.hpp"

FFN::FFN(){
}

void FFN::add_layer(Layer *l){
    layers.push_back(l);
}

void FFN::initFFN(int nb_inputs_neurons, int nb_hidden_neurons, int nb_outputs_neurons){
    this->add_layer(new Layer(0, nb_inputs_neurons, this));
    this->add_layer(new Layer(1, nb_hidden_neurons, this));
    this->add_layer(new Layer(2, nb_outputs_neurons, this));
}

void FFN::sim(vector<double> inputs){
    layers[0]->set_inputs(inputs);
    for(int indice = 0; indice < this->get_nb_layers(); indice++){
        this->get_layer_at(indice)->forward_propagate();
    }
}

void FFN::test(vector<vector<double>> Xtest, vector<vector<double>> Tt, vector<double> &mean_error){
    vector<double> current_input;
    vector<double> current_output;
    vector<vector<double>> error;
    vector<double> total_error(Tt[0].size(), 0);

    vector<double> zeros(Tt[0].size(), 0);
    
    for(int i=0; i<Xtest.size(); i++){
        current_input = Xtest[i];
        this->sim(current_input);
        current_output = this->get_ffn_outputs();
        error.push_back(zeros);
        for(size_t j = 0; j < current_output.size(); ++j){
            error[i][j] = abs(current_output[j] - Tt[i][j]);
            total_error[j] += error[i][j];
        }

    }
    for(size_t j = 0; j < total_error.size(); ++j){
        total_error[j] = total_error[j] / Xtest.size();
    }

    // cout << "Mean Error: ";
    // for(size_t j = 0; j < total_error.size(); ++j){
    //     if(j == total_error.size() - 1){
    //         cout << total_error[j] << "\n";
    //     } else {
    //         cout << total_error[j] << ", ";
    //     }
    // }

    mean_error = total_error;
}

void FFN::train_by_error(vector<vector<double>> inputs, vector<vector<double>> targets, double target_error){
    double error;
    do{
        error = 0;
        for(int i=0; i<inputs.size(); i++){
            
            layers[0]->set_inputs(inputs[i]);
            this->set_targets(targets[i]);
            for(int indice = 0; indice < this->get_nb_layers(); indice++){
                this->get_layer_at(indice)->forward_propagate();
                
            }
            for(int j=0; j<targets[i].size();j++){
                error += (targets[i][j]-this->get_ffn_outputs()[j])*(targets[i][j]-this->get_ffn_outputs()[j]);
            }
            
            for(int indice = this->get_nb_layers()-1; indice >=0; indice--){
                this->get_layer_at(indice)->calc_deltas();
                this->get_layer_at(indice)->calc_new_weights();
            }
        }
        
        
    }while(error>target_error);
    cout << "Stopping at error : " << error << endl;
}

void FFN::train_by_iteration(vector<vector<double>> inputs, vector<vector<double>> targets, int target_iteration){
    int ite = 0;
    do{
        ite++;
        //error = 0;
        for(int i=0; i<inputs.size(); i++){
            
            layers[0]->set_inputs(inputs[i]);
            this->set_targets(targets[i]);
            for(int indice = 0; indice < this->get_nb_layers(); indice++){
                this->get_layer_at(indice)->forward_propagate();
                
            }
            //for(int j=0; j<targets[i].size();j++){
                //error += (targets[i][j]-this->get_ffn_outputs()[j])*(targets[i][j]-this->get_ffn_outputs()[j]);
            //}
            
            for(int indice = this->get_nb_layers()-1; indice >=0; indice--){
                this->get_layer_at(indice)->calc_deltas();
                this->get_layer_at(indice)->calc_new_weights();
            }
        }
        
    }while(ite<target_iteration);
    // cout << "Stopping at iteration : " << ite << endl;
}

void FFN::about(){
    cout << "Number of layers : "<< layers.size() << endl;
    cout << "Number of input neurons : "<< layers[0]->get_nb_neurons() << endl;
    cout << "Number of hidden neurons : "<< layers[1]->get_nb_neurons() << endl;
    cout << "Number of output neurons : "<< layers[2]->get_nb_neurons() << endl;
}

vector<double> FFN::get_ffn_outputs(){
    return this->get_layer_at(this->get_nb_layers()-1)->get_outputs();
}

void FFN::set_targets(vector<double> tar){
    targets = tar;
}

void FFN::show_full_weights_matrix(){
    for(size_t i = 1; i < this->get_nb_layers(); ++i){
        this->get_layer_at(i)->show_weights_matrix();
        cout << endl;
    }
}
    
void FFN::get_weights_matrix(vector<vector<double>> &weight, int i){
    weight = this->get_layer_at(i)->get_weights();
}