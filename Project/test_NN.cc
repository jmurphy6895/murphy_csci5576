#include "kepler.h"
//#include "lambert.h"
#include "Header.h"
#include <cassert>
#include <math.h>


int main(int argc, char** argv){

    assert (argc>1);

    MPI_Init(&argc, &argv);

    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    double t1, t2, t3, t4;
    t1 = MPI_Wtime();

    int size;
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    if(rank == 0) {
        std::cout<<"Number of Processors: "<< size << std::endl;
    }

    MPI_Comm grid_comm;

    int n = atoi(argv[1]);
    int num_neurons_hid = 20;

    vector<vector<double>> train_x, train_y, test_x, test_y;
    vector<double> mean_error;

    generate_Kepler_data(train_x, train_y, test_x, test_y, n/size);

    FFN *KEPLERnetwork = new FFN();
    KEPLERnetwork->initFFN(2, num_neurons_hid, 1);
    //KEPLERnetwork->about();

    t2 = MPI_Wtime();

    //if (rank == 0) KEPLERnetwork->show_full_weights_matrix();

    for(size_t i = 0; i < 1000; ++i){
        KEPLERnetwork->train_by_iteration(train_x, train_y, 1);

        for(size_t j = 1; j < KEPLERnetwork->get_nb_layers(); ++j){
            vector<vector<double>> layer_weights;
            
            KEPLERnetwork->get_weights_matrix(layer_weights, j);
            
            vector<vector<double>> global_layer_weights;
            global_layer_weights.resize(layer_weights.size(), vector<double>(layer_weights[0].size(), 0.0));
            
            for(size_t k = 0; k < layer_weights.size(); ++k){
                MPI_Reduce(&layer_weights[k].front(), &global_layer_weights[k].front(), layer_weights[k].size(), MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
            }
            
            if(rank == 0){
                for(size_t k = 0; k < global_layer_weights.size(); ++k){
                    for(size_t l = 0; l < global_layer_weights[0].size(); ++l){
                        global_layer_weights[k][l] = global_layer_weights[k][l] / size;
                    }
                }
            }
            
            for(size_t k = 0; k < global_layer_weights.size(); ++k){
                MPI_Bcast(&global_layer_weights[k].front(), global_layer_weights[k].size(), MPI_DOUBLE, 0, MPI_COMM_WORLD);
            }

            KEPLERnetwork->get_layer_at(j)->set_weights(global_layer_weights);
            
        }

    }

    //if (rank == 0) KEPLERnetwork->show_full_weights_matrix();

    t3 = MPI_Wtime();

    KEPLERnetwork->test(test_x, test_y, mean_error);

    std::vector<double> total_mean_error(mean_error.size(), 0.0);
    MPI_Reduce(&mean_error.front(), &total_mean_error.front(), mean_error.size(), MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);

    for(size_t i = 0; i < total_mean_error.size(); ++i){
        total_mean_error[i] = total_mean_error[i] / size;
    }
    
    t4 = MPI_Wtime(); 

    if(rank == 0) {
        std::cout << "Total Mean Error: ";
        for(size_t j = 0; j < total_mean_error.size(); ++j){
            if(j == total_mean_error.size() - 1){
                std::cout << total_mean_error[j] << std::endl;
            } else {
                std::cout << total_mean_error[j] << ", ";
            }
        }

        std::cout << "Total Elapsed time is " << t4 - t1 << std::endl;
        std::cout << "Total Data Geneteration time is " << t2 - t1 << std::endl;
        std::cout << "Total Training time is " << t3 - t2 << std::endl;
        std::cout << "Total Testing time is " << t4 - t3 << std::endl;
    }

    // clean up
    MPI_Finalize();

    return 0;
}