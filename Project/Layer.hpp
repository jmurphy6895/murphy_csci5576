//
//  Layer.hpp
//  Neural Networks
//
//  Created by Alexis Louis on 15/03/2016.
//  Copyright © 2016 Alexis Louis. All rights reserved.
//

#ifndef Layer_hpp
#define Layer_hpp

#include "Header.h"

using namespace std;
class FFN;

class Layer
{
public:
    Layer(int indice, int nb_neurons, FFN *net);
    void forward_propagate(void);
    void back_propagate(void);

    vector<double> get_outputs(){return outputs;};
    vector<double> get_inputs(){return inputs;};
    vector<double> get_deltas(){return deltas;};
    vector<vector<double>> get_weights(){return weights;};
    void set_weights(vector<vector<double>> weight);
    int get_nb_neurons(){return nb_neurons;};
    int get_indice(){return indice;};
    
    void set_outputs(vector<double> out);
    void set_inputs(vector<double> in);

    void show_weights_matrix();
    void calc_deltas();
    void calc_new_weights();



private:
    FFN *network;
    int indice;
    int nb_neurons;
    vector<vector<double>> weights;

//ForwardProp
    vector<double> inputs;
    vector<double> outputs;
    void calc_inputs();
    void calc_outputs();

//Back Propagation
    vector<double> deltas;


};

#endif /* Layer_hpp */
