#ifndef ___KEPLER__
#define ___KEPLER__

#include <vector>
#include <algorithm>
#include <iostream>
#include <mpi.h>
#include <random>
#include <fstream>
#include <cmath>

using namespace std;

#define PI 3.14159265

/*
 *  Creates Random Selection of Eccentric Anomalies(E) and Eccentricities(e) used to Calculate Mean Anomalies(M)
 */ 
void generate_Kepler_data(vector<vector<double>> &train_x, vector<vector<double>> &train_y, vector<vector<double>> &test_x, vector<vector<double>> &test_y, const int n);

#endif