#!/bin/bash

#SBATCH --time=0:05:00
#SBATCH --nodes=1
#SBATCH -o axpy-%j.out
#SBATCH -e axpy-%j.err
#SBATCH --ntasks 16
#SBATCH --qos debug

# run the program
echo "Running on $(hostname --fqdn)"
module load intel

export OMP_NUM_THREADS=16

./test_axpy.exe
