#include <mpi.h>
#include <stdio.h>
#include "slurp_file.h"
#include <cassert>
#include <climits>

int main(int argc, char** argv){

	assert (argc>1);

	MPI_Init(&argc, &argv);

	int rank;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	std::vector<int> data;

	slurp_file_line(argv[1], rank, data);

	int max = INT_MIN;

	for(size_t i = 0; i < data.size(); ++i){
		if(data[i] > max){
			max = data[i];
		}
	}

	printf("Maximum from row %d is %d\n", rank, max);

	MPI_Finalize();

	return 0;

}
