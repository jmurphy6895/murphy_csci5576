#include <mpi.h>
#include <stdio.h>
#include "slurp_file.h"
#include <cassert>
#include <climits>

int main(int argc, char** argv){

	assert (argc>1);

	MPI_Init(&argc, &argv);

	int rank;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	int size;
	MPI_Comm_size(MPI_COMM_WORLD, &size);

	std::vector<int> col;
	std::vector<int> x;

	slurp_file_line(argv[1], rank, col);
	slurp_file_line(argv[1], size, x);

	std::vector<int> Ax(x.size(), 0);

	for(size_t i = 0; i < col.size(); ++i){
		Ax[i] = col[i] * x[rank];
	}
	
	if(rank != 0){
		MPI_Send(&Ax.front(), Ax.size(), MPI_INT, 0, 0, MPI_COMM_WORLD);
	} 
	else {
		MPI_Status st;
		for(size_t i = 1; i < size; ++i){
			std::vector<int> temp(Ax.size(), 0);
			MPI_Recv(&temp.front(), Ax.size(), MPI_INT, i, 0, MPI_COMM_WORLD, &st);
			for(size_t j = 0; j < temp.size(); ++j){
				Ax[j] += temp[j];
			}
		}
	}

	if(rank == 0){
		printf("Ax = [");

		for(size_t i = 0; i < Ax.size(); ++i){
			if(i != Ax.size() - 1){
				printf("%d; ", Ax[i]);
			}
			else{
				printf("%d]\n", Ax[i]);
			
			}
		}
	}

	MPI_Finalize();

	return 0;

}