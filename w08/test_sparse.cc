#include "sparse.h"
#include <cassert>

#define PRINT

int main(int argc, char** argv){

    assert (argc>3);
    // Number of Rows
    size_t m = atoi(argv[1]);
    // Number of Columns
    size_t n = atoi(argv[2]);
    // Maximum number of non-zero entries
    size_t nnz = atoi(argv[3]);

    MPI_Init(&argc, &argv);

    double t1, t2;
    t1 = MPI_Wtime();

    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    int size;
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    if(rank == 0) {
        std::cout<<"Number of Processors: "<<size<<std::endl;
    }

    sparse A(m / size, n);
    sparse B(n, m / size);

    // Randomly Fill a Sparse Matrix
    A.fill_CSR(nnz / size);

#ifdef PRINT
    // Print Functions for Debugging
    A.print();
#endif

    // Convert the Matrix to COO
    A.convert2COO(); 

    // Transpose the A Matrix
    A.transpose(B);

    // Convert the B Matrix to CSR
    B.convert2CSR();

#ifdef PRINT
    // Print Functions for Debugging
    B.print();
#endif

    t2 = MPI_Wtime(); 
    if(rank == 0) {
        std::cout<<"Elapsed time is "<<t2 - t1<<std::endl;
    }

    // clean up
    MPI_Finalize();

    return 0;
}