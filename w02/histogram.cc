/**
 * @file histogram.cc
 * @author Jordan Murphy
 * @date 9/10/18
 */
#include "histogram.h"


void histogram(const vec_t &x, int num_bins, vec_t &bin_bdry, count_t &bin_count){

	double mn, mx, spacing;
	
	#pragma omp parallel
	{
		#pragma omp task
			mn = find_min(x);
		#pragma omp task
			mx = find_max(x);
	}
		
	spacing = (mx - mn) / num_bins;

	#pragma omp parallel for
	for(size_t i = 0; i < num_bins; ++i)
	{
		bin_bdry[i] = (mn + i * spacing);
	}

	bin_bdry[0] *= (1-1E-14);
	bin_bdry[num_bins]= mx * (1+1E-14);
		
	#pragma omp parallel for collapse(2)
	for (size_t i = 0; i < x.size(); ++i)
	{
		for (size_t j = 0; j < num_bins; ++j)
		{
			if(x[i] >= bin_bdry[j] && x[i] < bin_bdry[j+1]){
				bin_count[j]++;
			}		
		}
	}
}

double find_max(const vec_t &x){

	bool temp[x.size()];
	double max;

	#pragma omp parallel for 
	for (size_t i = 0; i < x.size(); ++i)
	{
		temp[i] = true;
	}

	#pragma omp parallel for collapse(2)
	for (size_t i = 0; i < x.size(); ++i)
	{
		for (size_t j = 0; j < x.size(); ++j)
		{
			if(x[i] < x[j]){
				temp[i] = false;
			}
		}
	}

	#pragma omp parallel for
	for (size_t i = 0; i < x.size(); ++i)
	{
		if(temp[i]){
			max = x[i];
		}
	}

	return max;
}

double find_min(const vec_t &x){

	bool temp[x.size()];
	double min;

	#pragma omp parallel for 
	for (size_t i = 0; i < x.size(); ++i)
	{
		temp[i] = true;
	}

	#pragma omp parallel for collapse(2)
	for (size_t i = 0; i < x.size(); ++i)
	{
		for (size_t j = 0; j < x.size(); ++j)
		{
			if(x[i] > x[j]){
				temp[i] = false;
			}
		}
	}

	#pragma omp parallel for
	for (size_t i = 0; i < x.size(); ++i)
	{
		if(temp[i]){
			min = x[i];
		}
	}

	return min;

}