/**
 * @file
 * @author Rahimian, Abtin <arahimian@acm.org>
 * @revision $Revision: 25 $
 * @tags $Tags: tip $
 * @date $Date: Thu Jan 01 00:00:00 1970 +0000 $
 */

#include "poisson.h"
#include <iostream>
#include <cmath>

void poisson_setup(const MPI_Comm &comm, int n, MPI_Comm &grid_comm,grid_t &x)
{
    int rank;
	MPI_Comm_rank(comm, &rank);

	int size;
    MPI_Comm_size(comm, &size);

	if(rank == 0) { std::cout<<"setup"<<std::endl; }

	int dims[DIM] = {};
	MPI_Dims_create(size, DIM, dims);

	int periods[DIM] = {};
	int reorder = 0;
	MPI_Cart_create(comm, DIM, dims, periods, reorder, &grid_comm);

	int coords[DIM] = {};
	MPI_Cart_coords(grid_comm, rank, DIM, coords);

	real_t h = 1.0/(n - 1);

	for(size_t i = 0; i < n/dims[0]; ++i){
		for(size_t j = 0; j < n/dims[1]; ++j){
			for(size_t k = 0; k < n/dims[2]; ++k){
				point_t temp;
				temp.x = (coords[0]*(n/dims[0]) + i)*h;
				temp.y = (coords[1]*(n/dims[1]) + j)*h;
				temp.z = (coords[2]*(n/dims[2]) + k)*h;
				x.push_back(temp);
			}
		}
	}
}

void poisson_matvec(const MPI_Comm &grid_comm, int n, const vec_t &a, const vec_t &v, vec_t &lv)
{
    int rank;
	MPI_Comm_rank(grid_comm, &rank);

	int size;
    MPI_Comm_size(grid_comm, &size);

    int source, dest;
    MPI_Status st;

    if(rank == 0) { std::cout<<"matvec"<<std::endl; }

    int dims[DIM] = {};
	MPI_Dims_create(size, DIM, dims);
	for (size_t i = 0; i < DIM; ++i){
		dims[i] = n/dims[i];
	}

    int coords[DIM] = {};
	MPI_Cart_coords(grid_comm, rank, DIM, coords);

	int index[DIM] = {};

	real_t h = 1.0/(n - 1);   

	vec_t right_r, left_r, up_r, down_r, bottom_r, top_r;

	MPI_Cart_shift(grid_comm, 0, -1, &source, &dest);
	if(dest != MPI_PROC_NULL) {
    	vec_t left;
    	for(size_t y = 0; y < dims[1]; ++y){
    		for(size_t z = 0; z < dims[2]; ++z){
    			int index = convert2index(0, y, z, dims);
    			left.push_back(v[index]);
    		}
    	}
    	MPI_Sendrecv(&left.front(), dims[1]*dims[2], MPI_DOUBLE, dest, 0,
    				 &right_r, dims[1]*dims[2], MPI_DOUBLE, source, 0,
    				 grid_comm, &st);
    }

 	MPI_Cart_shift(grid_comm, 0, 1, &source, &dest);
    if(dest != MPI_PROC_NULL) {
    	vec_t right;
    	for(size_t y = 0; y < dims[1]; ++y){
    		for(size_t z = 0; z < dims[2]; ++z){
    			int index = convert2index(dims[0] - 1, y, z, dims);
    			right.push_back(v[index]);
    		}
    	}
    	MPI_Sendrecv(&right.front(), dims[1]*dims[2], MPI_DOUBLE, dest, 0,
    				 &left_r, dims[1]*dims[2], MPI_DOUBLE, source, 0,
    				 grid_comm, &st);
    }

	MPI_Cart_shift(grid_comm, 1, -1, &source, &dest);
    if(dest != MPI_PROC_NULL) {
    	vec_t up;
    	for(size_t x = 0; x < dims[0]; ++x){
    		for(size_t z = 0; z < dims[2]; ++z){
    			int index = convert2index(x, 0, z, dims);
    			up.push_back(v[index]);
    		}
    	}
    	MPI_Sendrecv(&up.front(), dims[0]*dims[2], MPI_DOUBLE, dest, 0,
    				 &down_r, dims[0]*dims[2], MPI_DOUBLE, source, 0,
    				 grid_comm, &st);
    }

    MPI_Cart_shift(grid_comm, 1, 1, &source, &dest);
    if(dest != MPI_PROC_NULL) {
    	vec_t down;
    	for(size_t x = 0; x < dims[0]; ++x){
    		for(size_t z = 0; z < dims[2]; ++z){
    			int index = convert2index(x, dims[1] - 1, z, dims);
    			down.push_back(v[index]);
    		}
    	}
    	MPI_Sendrecv(&down.front(), dims[0]*dims[2], MPI_DOUBLE, dest, 0,
    				 &up_r, dims[0]*dims[2], MPI_DOUBLE, source, 0,
    				 grid_comm, &st);
    }

    MPI_Cart_shift(grid_comm, 2, -1, &source, &dest);
    if(dest != MPI_PROC_NULL) {
    	vec_t top;
    	for(size_t x = 0; x < dims[0]; ++x){
    		for(size_t y = 0; y < dims[1]; ++y){
    			int index = convert2index(x, y, 0, dims);
    			top.push_back(v[index]);
    		}
    	}
    	MPI_Sendrecv(&top.front(), dims[0]*dims[1], MPI_DOUBLE, dest, 0,
    				 &bottom_r, dims[0]*dims[1], MPI_DOUBLE, source, 0,
    				 grid_comm, &st);
    }

	MPI_Cart_shift(grid_comm, 2, 1, &source, &dest);
    if(dest != MPI_PROC_NULL) {
    	vec_t bottom;
    	for(size_t x = 0; x < dims[0]; ++x){
    		for(size_t y = 0; y < dims[1]; ++y){
    			int index = convert2index(x, y, dims[2] - 1, dims);
    			bottom.push_back(v[index]);
    		}
    	}
    	MPI_Sendrecv(&bottom.front(), dims[0]*dims[1], MPI_DOUBLE, dest, 0,
    				 &top_r, dims[0]*dims[1], MPI_DOUBLE, source, 0,
    				 grid_comm, &st);
    }

	int right_count = 0;
	int left_count = 0;
	int down_count = 0;
	int up_count = 0;
	int bottom_count = 0;
	int top_count = 0;

    for(size_t i = 0; i < v.size(); ++i){

    	int* index = convert23D(i, dims);

    	real_t a_u = a[i] * v[i];
    	real_t lap_u = 0;

    	if (index[0] == 0) {
    		MPI_Cart_shift(grid_comm, 0, -1, &source, &dest);
    		if(dest != MPI_PROC_NULL) {
    			lap_u += right_r[right_count];
    			right_count++;
    		}

    	} else {
    		lap_u += v[i - 1];
    	}

    	if (index[0] == dims[0] - 1) {
    		MPI_Cart_shift(grid_comm, 0, 1, &source, &dest);
    		if(dest != MPI_PROC_NULL) {
    			lap_u += left_r[left_count];
    			left_count++;
    		}

    	} else {
    		lap_u += v[i + 1];
    	}

    	if (index[1] == 0) {
    		MPI_Cart_shift(grid_comm, 1, -1, &source, &dest);
    		if(dest != MPI_PROC_NULL) {
    			lap_u += down_r[down_count];
    			down_count++;
    		}

    	} else {
    		lap_u += v[i - dims[0]];
    	}

    	if (index[1] == dims[1] - 1) {
    		MPI_Cart_shift(grid_comm, 1, 1, &source, &dest);
    		if(dest != MPI_PROC_NULL) {
    			lap_u += up_r[up_count];
    			up_count++;
    		}

    	} else {
    		lap_u += v[i + dims[0]];
    	}

    	if (index[2] == 0) {
    		MPI_Cart_shift(grid_comm, 2, -1, &source, &dest);
    		if(dest != MPI_PROC_NULL) {
    			lap_u += bottom_r[bottom_count];
    			bottom_count++;
    		}

    	} else {
    		lap_u += v[i - dims[0] * dims[1]];
    	}

    	if (index[2] == dims[2] - 1) {
    		MPI_Cart_shift(grid_comm, 2, 1, &source, &dest);
    		if(dest != MPI_PROC_NULL) {
    			lap_u += top_r[top_count];
    			top_count++;
    		}

    	} else {
    		lap_u += v[i + dims[0] * dims[1]];
    	}

    	lap_u -= 6 * v[i];
    	lap_u = lap_u / pow(h, 2);

    	lv.push_back(a_u - lap_u);
    }

}

void residual(const MPI_Comm &comm, matvec_t &mv, const vec_t &v, const vec_t &rhs,
              vec_t &res, real_t &res_norm)
{
    int rank;
	MPI_Comm_rank(comm, &rank);

    if(rank == 0) { std::cout<<"residual"<<std::endl; }

    real_t local_res_norm = 0;
    vec_t lv;

    mv(v, lv);

    for(size_t i = 0; i < v.size(); ++i){
    	res.push_back(rhs[i] - lv[i]);
    	local_res_norm += pow(res[i], 2);
    }

    std::cout << "Processor: " << rank << "Made it";

    MPI_Reduce(&local_res_norm, &res_norm, 1, MPI::DOUBLE, MPI_SUM, 0, comm);
    if(rank == 0) { res_norm = pow(res_norm, 1/2); }
}


int* convert23D(int i, int dims[]){
	int * arr = new int[3];
	arr[2] = i / (dims[0] * dims[1]);
	i -= arr[2] * dims[0] * dims[1];
	arr[1] = i / dims[0];
	arr[0] = i % dims[0];
	return arr;
}

int convert2index(int x, int y, int z, int dims[]){
	return x + y * dims[0] + z * dims[0] * dims[1];
}