#!/bin/bash

#SBATCH --time=0:05:00
#SBATCH --nodes=1
#SBATCH -o histogram-%j.out
#SBATCH -e histogram-%j.err
#SBATCH --ntasks 1
#SBATCH --qos debug

# run the program
echo "Running on $(hostname --fqdn)"
module load intel

export OMP_NUM_THREADS=1

./test_histogram.exe 2 1
