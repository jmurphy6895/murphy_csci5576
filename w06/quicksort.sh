#!/bin/bash

#SBATCH --time=0:30:00
#SBATCH --nodes=1
#SBATCH -o quicksort-%j.out
#SBATCH -e quicksort-%j.err
#SBATCH --ntasks 24
#SBATCH --cpus-per-task 24
#SBATCH --qos debug

# run the program
echo "Running on $(hostname --fqdn)"
module load intel

# Strong scaling
for i in {1,2,4,8,12,16,20,24};do export OMP_NUM_THREADS=$i; ./test_quicksort.exe 1000000;done
# Weak scaling
for i in {1,2,4,8,12,16,20,24};do export OMP_NUM_THREADS=$i; ./test_quicksort.exe $((1000000*$i));done


# Strong scaling
for i in {1,2,4,8,12,16,20,24};do export OMP_NUM_THREADS=$i; ./test_quicksort.exe 10000000;done
# Weak scaling
for i in {1,2,4,8,12,16,20,24};do export OMP_NUM_THREADS=$i; ./test_quicksort.exe $((10000000*$i));done