#Task 1

1. PATH -- The search path for commands.  It is a colon-separated list of directories in which the shell looks for commands (see COMMAND EXECUTION below).  A zero-length (null) directory name in the value of PATH  indicates  the current  directory.   A  null  directory  name may appear as two adjacent colons, or as an initial or trailing colon.  The default path is system-dependent, and is set by the administrator who installs bash.  A common value is "/usr/gnu/bin:/usr/local/bin:/usr/ucb:/bin:/usr/bin"

2. LD_LIBRARY_PATH -- 5. For a native linker, search the contents of the environment variable "LD_LIBRARY_PATH".

3. Value Stored in HOME: /home/jomu9873

#Task 2

1. Before Loading Intel Module

	* PATH: /usr/lpp/mmfs/bin:/usr/local/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/opt/dell/srvadmin/bin
	* LD_LIBRARY_PATH: Blank Variable

2. After Loading Intel Module

	* PATH: /curc/sw/intel/17.4/compilers_and_libraries_2017.4.196/linux/bin/intel64:/curc/sw/gcc/5.4.0/bin:/usr/lpp/mmfs/bin:/usr/local/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/opt/dell/srvadmin/bin
	* LD_LIBRARY_PATH: /curc/sw/intel/17.4/compilers_and_libraries_2017.4.196/linux/compiler/lib/intel64:/curc/sw/gcc/5.4.0/lib64


3. After the intel module is loaded the list of available modules expands to show the modules dependent on intel in addition to the independent modules.

4. The motivation behind the hierarchical module system is to ensure that all of the loaded modules dependencies are loaded first, ensuring proper execution of all stages.

#Task 3

1. The intel compiler sets the environment variables: CC, FC, CXX, AR, LD

2. The gcc compiler sets the environment variables: CC, FC, CXX

#Task 4

1. Output from \${CC} -v : icc version 17.0.4 (gcc version 5.4.0 compatibility)

2. Command to link and compile code: icc -o test_axpy.exe test_axpy.cc axpy.cc

#Task 5

1. Command used to run job on Summit: sbatch job.sh

2. Output of .err file: None

3. Output of .out file: Calling axpy - Passed

#Task 6

1. The compiler gives warning 3180: unrecognized OpenMP #pragma which can be fixed by adding the -fopenmp flag to the compiler command. The output of the program now also outputs an elapsed time.

2. As the number of threads are increased the elapsed time begins dropping:

	* Number of Threads = 1 --- Elapsed Time: .231287
	* Number of Threads = 2 --- Elapsed Time: .129631
	* Number of Threads = 4 --- Elapsed Time: .0682509
	* Number of Threads = 8 --- Elapsed Time: .0632091
	* Number of Threads = 16 --- Elapsed Time: .056905