/**
 * @file
 * @author Rahimian, Abtin <arahimian@acm.org>
 * @revision $Revision$
 * @tags $Tags$
 * @date $Date$
 *
 * @brief
 */

#ifndef _MESSAGE_H_
#define _MESSAGE_H_

class message{
  public:
    void printMessage();
};

#endif //_MESSAGE_H_
