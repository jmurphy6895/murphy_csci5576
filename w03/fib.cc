#include "fib.h"

long fib(long n) {
	long i(0), j(0);
	if (n<2) return 1;

	#pragma omp task shared(i)
	i = fib(n-1);

	#pragma omp task shared(j)
	j = fib(n-2);

	#pragma omp taskwait
	return i + j;
}