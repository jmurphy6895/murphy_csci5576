#!/bin/bash

#SBATCH --time=0:30:00
#SBATCH -o possion-%j.out
#SBATCH -e possion-%j.err
#SBATCH --qos debug

# advise task manager that maximum of 64
# tasks/processes may be spawned
#SBATCH --ntasks 64

# run the program
for i in {8, 27, 64};do mpirun -n i ./test_poisson.exe $((300*$i));done