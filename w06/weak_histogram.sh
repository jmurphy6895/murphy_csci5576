#!/bin/bash

#SBATCH --time=0:30:00
#SBATCH --nodes=1
#SBATCH -o histogram-%j.out
#SBATCH -e histogram-%j.err
#SBATCH --ntasks 24
#SBATCH --cpus-per-task 24
#SBATCH --qos debug

# run the program
echo "Weak -- Running on $(hostname --fqdn)"
module load intel

# Weak scaling
for i in {1,2,4,8,12,16,20,24};do export OMP_NUM_THREADS=$i; ./test_histogram.exe 2 1;done
for i in {1,2,4,8,12,16,20,24};do export OMP_NUM_THREADS=$i; ./test_histogram.exe 2 3;done
for i in {1,2,4,8,12,16,20,24};do export OMP_NUM_THREADS=$i; ./test_histogram.exe 2 4;done

for i in {1,2,4,8,12,16,20,24};do export OMP_NUM_THREADS=$i; ./test_histogram.exe 5 1;done
for i in {1,2,4,8,12,16,20,24};do export OMP_NUM_THREADS=$i; ./test_histogram.exe 5 3;done
for i in {1,2,4,8,12,16,20,24};do export OMP_NUM_THREADS=$i; ./test_histogram.exe 5 4;done

for i in {1,2,4,8,12,16,20,24};do export OMP_NUM_THREADS=$i; ./test_histogram.exe 9 1;done
for i in {1,2,4,8,12,16,20,24};do export OMP_NUM_THREADS=$i; ./test_histogram.exe 9 3;done
for i in {1,2,4,8,12,16,20,24};do export OMP_NUM_THREADS=$i; ./test_histogram.exe 9 4;done
