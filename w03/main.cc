/**
 * @file
 * @author Rahimian, Abtin <arahimian@acm.org>
 * @revision $Revision$
 * @tags $Tags$
 * @date $Date$
 *
 * @brief
 */

#include "message.h"
#include "fib.h"
#include "omp-utils.h"
#include <cstdio>
#include <cmath> 
#include <cstdlib> 
#include <cassert>

void demo_task(){
	long n(5), v;
	#pragma omp parallel shared (n, v)
	{
		#pragma omp single /* why single? */
		v = fib(n);
		if(omp_get_thread_num() == 0){
			printf("%d\n", v);
		}
	}
}

int main(){
  //message m;
  //m.printMessage();
  demo_task();

  return 0;
}


