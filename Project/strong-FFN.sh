#!/bin/bash

#SBATCH --time=0:30:00
#SBATCH -o strong-FFN-%j.out
#SBATCH -e strong-FFN-%j.err
#SBATCH --qos debug

# advise task manager that maximum of 100
# tasks/processes may be spawned
#SBATCH --ntasks 100

# run the program
mpirun -n 1 ./test_NN.exe 10000000
mpirun -n 2 ./test_NN.exe 10000000
mpirun -n 4 ./test_NN.exe 10000000
mpirun -n 5 ./test_NN.exe 10000000
mpirun -n 10 ./test_NN.exe 10000000
mpirun -n 20 ./test_NN.exe 10000000
mpirun -n 25 ./test_NN.exe 10000000
mpirun -n 50 ./test_NN.exe 10000000
mpirun -n 100 ./test_NN.exe 10000000